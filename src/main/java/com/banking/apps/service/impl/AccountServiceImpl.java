package com.banking.apps.service.impl;

import com.banking.apps.dto.AccountDto;
import com.banking.apps.entity.Account;
import com.banking.apps.mapper.AccountMapper;
import com.banking.apps.repository.AccountRepository;
import com.banking.apps.service.AccountService;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService {

    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public AccountDto createAccount(AccountDto accountDto) {
        Account accounts = AccountMapper.mapToAccount(accountDto);
        Account savedAccount = accountRepository.save(accounts);
        return AccountMapper.mapToAccountDto(savedAccount);
    }
}
