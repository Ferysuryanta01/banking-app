package com.banking.apps.service;

import com.banking.apps.dto.AccountDto;

public interface AccountService {

    AccountDto createAccount(AccountDto account);
}
