package com.banking.apps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppBankingApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppBankingApplication.class, args);
	}

}
